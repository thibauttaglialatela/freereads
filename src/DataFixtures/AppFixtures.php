<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserBook;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory as Faker;

class AppFixtures extends Fixture
{
    public const STATUS = ['to-read', 'reading', 'read'];

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker::create('fr_FR');

        // création de 10 auteurs
        $authors = [];

        for ($i = 0; $i < 10; ++$i) {
            $author = new Author();
            $author->setName($faker->name());
            $manager->persist($author);
            array_push($authors, $author);
        }

        // créer 10 maisons d'édition
        $publishers = [];

        for ($i = 0; $i < 10; ++$i) {
            $publisher = new Publisher();
            $publisher->setName($faker->company());
            $manager->persist($publisher);
            $publishers[] = $publisher;
        }

        // créer les statuts de lecture
        $status = [];
        foreach (self::STATUS as $value) {
            $oneStatus = new Status();
            $oneStatus->setName($value);
            $manager->persist($oneStatus);
            $status[] = $oneStatus;
        }

        // création de 100 livres
        $books = [];
        for ($i = 0; $i < 100; ++$i) {
            $book = new Book();
            $book
                ->setGoogleBooksId($faker->uuid())
                ->setTitle($faker->sentence())
                ->setSubtitle($faker->sentence())
                ->setDescription($faker->realText())
                ->setIsbn13($faker->isbn13())
                ->setIsbn10($faker->isbn10())
                ->setPageCount($faker->numberBetween(100, 1000))
                ->setThumbnail('https://picsum.photos/300/200')
                ->setSmallThumbnail('https://picsum.photos/150/100')
                ->setPublishDate($faker->dateTime())
                ->addAuthor($faker->randomElement($authors))
                ->addPublisher($faker->randomElement($publishers));
            $manager->persist($book);
            $books[] = $book;
        }

        // créer 10 utilisateurs
        $users = [];
        for ($i = 0; $i < 10; ++$i) {
            $user = new User();
            $user->setEmail($faker->email())
                ->setPassword($faker->password())
                ->setPseudo($faker->userName());
            $manager->persist($user);
            $users[] = $user;
        }

        // créer 10 userbooks par user
        foreach ($users as $user) {
            for ($i = 0; $i < 10; ++$i) {
                $userBook = new UserBook();
                $userBook
                    ->setBook($faker->randomElement($books))
                    ->setReader($user)
                    ->setComment($faker->realText())
                    ->setRating($faker->numberBetween(0, 5))
                    ->setStatus($faker->randomElement($status))
                    ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()))
                    ->setUpdatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()));
                $manager->persist($userBook);
            }
        }

        $manager->flush();
    }
}
